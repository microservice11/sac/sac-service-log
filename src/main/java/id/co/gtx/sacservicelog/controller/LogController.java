package id.co.gtx.sacservicelog.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicelog.entity.Logs;
import id.co.gtx.sacservicelog.service.LogService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/log/data")
public class LogController {

    private final LogService logService;

    public LogController(LogService logService) {
        this.logService = logService;
    }

    @GetMapping("")
    public ResponseEntity<Mono<DtoResponse<List<Logs>>>> getLogAll() {
        return ResponseEntity.status(HttpStatus.OK).body(logService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Mono<DtoResponse<Logs>>> getLogById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(logService.findById(id));
    }
}
