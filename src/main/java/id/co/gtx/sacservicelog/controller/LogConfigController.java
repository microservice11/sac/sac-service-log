package id.co.gtx.sacservicelog.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicelog.entity.LogConfig;
import id.co.gtx.sacservicelog.service.LogConfigService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/log/config")
public class LogConfigController {

    private final LogConfigService logConfigService;

    public LogConfigController(LogConfigService logConfigService) {
        this.logConfigService = logConfigService;
    }

    @GetMapping("")
    public ResponseEntity<Mono<DtoResponse<List<LogConfig>>>> getLogConfigAll() {
        return ResponseEntity.status(HttpStatus.OK).body(logConfigService.findAll());
    }

    @GetMapping("/{id}/username/{username}")
    public ResponseEntity<Mono<DtoResponse<LogConfig>>> getLogConfigByIdAndUsername(@PathVariable Long id, @PathVariable String username) {
        return ResponseEntity.status(HttpStatus.OK).body(logConfigService.findById(id, username));
    }

    @GetMapping("/username/{username}")
    public ResponseEntity<Mono<DtoResponse<List<LogConfig>>>> getLogConfigByUsername(@PathVariable String username) {
        return ResponseEntity.status(HttpStatus.OK).body(logConfigService.findByUsername(username));
    }

    @GetMapping("/limit/{limit}")
    public ResponseEntity<Mono<DtoResponse<List<LogConfig>>>> getLogConfigLimit(@PathVariable Integer limit) {
        return ResponseEntity.status(HttpStatus.OK).body(logConfigService.findLimit(limit));
    }
}
