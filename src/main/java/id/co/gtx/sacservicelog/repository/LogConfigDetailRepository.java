package id.co.gtx.sacservicelog.repository;

import id.co.gtx.sacservicelog.entity.LogConfigDetail;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface LogConfigDetailRepository extends R2dbcRepository<LogConfigDetail, Long> {
    Flux<LogConfigDetail> findByLogConfigIdAndUsernameOrderByCreateAt(Long logId, String username);
}
