package id.co.gtx.sacservicelog.repository.query;

import id.co.gtx.sacservicelog.entity.Logs;
import id.co.gtx.sacservicelog.entity.LogsDetail;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.stream.Collectors;

@Repository
public class LogQueryRepository {

    private final DatabaseClient client;

    private final String sql = "select l.*, " +
            "   ld.id as ld_id, " +
            "   ld.colname as ld_colname, " +
            "   ld.new_value as ld_new_value, " +
            "   ld.old_value as ld_old_value " +
            "from logs l  " +
            "   inner join logs_detail ld on l.id = ld.log_id ";

    public LogQueryRepository(DatabaseClient client) {
        this.client = client;
    }

    private Date convertLocalDatetime(LocalDateTime source) {
        if (source != null)
            return Date.from(source.atZone(ZoneId.systemDefault()).toInstant());
        return null;
    }

    public Flux<Logs> mapping(DatabaseClient.GenericExecuteSpec executeSpec) {
        return executeSpec.fetch()
                .all()
                .bufferUntilChanged(result -> result.get("id"))
                .map(listLog -> Logs.builder()
                        .id((Long) listLog.get(0).get("id"))
                        .message((String) listLog.get(0).get("message"))
                        .operation((String) listLog.get(0).get("operation"))
                        .tbname((String) listLog.get(0).get("tbname"))
                        .tbkey((String) listLog.get(0).get("tbkey"))
                        .ipClient((String) listLog.get(0).get("ip_client"))
                        .username((String) listLog.get(0).get("username"))
                        .createAt((LocalDateTime) listLog.get(0).get("create_at"))
                        .logsDetails(listLog.stream()
                                .map(logsDetails -> LogsDetail.builder()
                                        .id((Long) logsDetails.get("ld_id"))
                                        .colname((String) logsDetails.get("ld_colname"))
                                        .newValue((String) logsDetails.get("ld_new_value"))
                                        .oldValue((String) logsDetails.get("ld_old_value"))
                                        .logId((Long) logsDetails.get("id"))
                                        .build())
                                .collect(Collectors.toList()))
                        .build());
    }

    public Flux<Logs> findAll() {
        String sql = this.sql + " order by l.create_at desc, ld.id desc";
        return mapping(client.sql(sql));
    }

    public Mono<Logs> findById(Long id) {
        String sql = this.sql + " where l.id = :id order by l.create_at desc, ld.id desc";
        return mapping(client.sql(sql)
                .bind("id", id)).next();
    }
}
