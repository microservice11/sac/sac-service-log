package id.co.gtx.sacservicelog.repository;

import id.co.gtx.sacservicelog.entity.LogsDetail;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface LogDetailRepository extends R2dbcRepository<LogsDetail, Long> {
    Flux<LogsDetail> findByLogId(Long id);
}
