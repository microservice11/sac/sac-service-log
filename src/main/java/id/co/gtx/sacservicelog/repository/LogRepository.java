package id.co.gtx.sacservicelog.repository;

import id.co.gtx.sacservicelog.entity.Logs;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.List;

@Repository
public interface LogRepository extends R2dbcRepository<Logs, Long> {
    Flux<Logs> findByIdIn(List<Long> id);
}
