package id.co.gtx.sacservicelog.repository.query;

import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacservicelog.entity.LogConfig;
import id.co.gtx.sacservicelog.entity.LogConfigDetail;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.stream.Collectors;

@Repository
public class LogConfigQueryRepoitory {

    private final DatabaseClient client;

    private final String sql = "select lc.*, " +
            "   concat(lc.id, lc.username) as lc_pk, " +
            "   lcd.id as lcd_id,  " +
            "   lcd.action as lcd_action,  " +
            "   lcd.target as lcd_target,  " +
            "   lcd.cmd as lcd_cmd, " +
            "   lcd.result as lcd_result, " +
            "   lcd.status as lcd_status, " +
            "   lcd.create_at as lcd_create_at " +
            "from log_config lc " +
            "   inner join log_config_detail lcd on lc.id = lcd.log_config_id " +
            "       and lc.username = lcd.username ";

    public LogConfigQueryRepoitory(DatabaseClient client) {
        this.client = client;
    }

    private Date convertLocalDatetime(LocalDateTime source) {
        if (source != null)
            return Date.from(source.atZone(ZoneId.systemDefault()).toInstant());
        return null;
    }

    public Flux<LogConfig> mapping(DatabaseClient.GenericExecuteSpec executeSpec) {
        return executeSpec.fetch()
                .all()
                .bufferUntilChanged(result -> result.get("lc_pk"))
                .map(listLogConfig -> LogConfig.builder()
                        .logId((Long) listLogConfig.get(0).get("id"))
                        .username((String) listLogConfig.get(0).get("username"))
                        .target((String) listLogConfig.get(0).get("target"))
                        .action((String) listLogConfig.get(0).get("action"))
                        .status(Enum.valueOf(Status.class, (String) listLogConfig.get(0).get("status")))
                        .type((Integer) listLogConfig.get(0).get("type"))
                        .ipClient((String) listLogConfig.get(0).get("ip_client"))
                        .startTime((LocalDateTime) listLogConfig.get(0).get("start_time"))
                        .stopTime((LocalDateTime) listLogConfig.get(0).get("stop_time"))
                        .logConfigDetails(listLogConfig.stream()
                                .map(logConfigDetails -> LogConfigDetail.builder()
                                        .id((Long) logConfigDetails.get("lcd_id"))
                                        .action((String) logConfigDetails.get("lcd_action"))
                                        .target((String) logConfigDetails.get("lcd_target"))
                                        .cmd((String) logConfigDetails.get("lcd_cmd"))
                                        .result(new String(Base64.getDecoder().decode((String) logConfigDetails.get("lcd_result"))))
                                        .status(Enum.valueOf(Status.class, (String) logConfigDetails.get("lcd_status")))
                                        .createAt((LocalDateTime) logConfigDetails.get("lcd_create_at"))
                                        .logConfigId((Long) logConfigDetails.get("id"))
                                        .username((String) logConfigDetails.get("username"))
                                        .build())
                                .collect(Collectors.toList()))
                        .build());
    }

    public Flux<LogConfig> findAll() {
        String sql = this.sql + " order by lc.start_time desc, lc.stop_time desc, lcd.create_at desc";
        return mapping(client.sql(sql));
    }

    public Flux<LogConfig> findAll(Integer limit) {
        return findAll(limit, 0);
    }

    public Flux<LogConfig> findAll(Integer limit, Integer offset) {
        String sql = this.sql + " where lc.id in (select log_config.id from log_config " +
                "order by log_config.start_time desc, log_config.stop_time desc " +
                "limit :limit offset :offset) " +
                "order by lc.start_time desc, lc.stop_time desc, lcd.create_at desc";

        return mapping(client.sql(sql)
                .bind("limit", limit)
                .bind("offset", offset));
    }

    public Flux<LogConfig> findByUsernameAndType(String username, Integer type) {
        String sql = this.sql + " where lc.username = :username and lc.type = :type " +
                "order by lc.start_time desc, lc.stop_time desc, lcd.create_at desc";
        return mapping(client.sql(sql)
                .bind("username", username)
                .bind("type", type));
    }

    public Mono<LogConfig> findByLogIdAndUsername(Long id, String username) {
        String sql = this.sql + " where lc.id = :id and lc.username = :username " +
                "order by lc.start_time desc, lc.stop_time desc, lcd.create_at desc";
        return mapping(client.sql(sql)
                .bind("id", id)
                .bind("username", username)).next();
    }
}
