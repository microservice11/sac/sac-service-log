package id.co.gtx.sacservicelog.repository;

import id.co.gtx.sacservicelog.entity.LogConfig;
import id.co.gtx.sacservicelog.entity.pk.LogConfigPK;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface LogConfigRepository extends R2dbcRepository<LogConfig, LogConfigPK> {
    Flux<LogConfig>  findByUsernameAndTypeOrderByStartTimeDesc(String username, Integer type);

    Mono<LogConfig> findByLogIdAndUsernameOrderByStartTimeDesc(Long logId, String username);

    @Modifying
    @Query("INSERT INTO public.log_config " +
            "(id, username, action, ip_client, start_time, status, stop_time, target, type) " +
            "VALUES(:#{#logConfig.logId}, :#{#logConfig.username}, :#{#logConfig.action}, :#{#logConfig.ipClient}, :#{#logConfig.startTime}, :#{#logConfig.status}, :#{#logConfig.stopTime}, :#{#logConfig.target}, :#{#logConfig.type})")
    Mono<Integer> saveCustom(LogConfig logConfig);

    @Modifying
    @Query("UPDATE public.log_config " +
            "SET action=:#{#logConfig.action}, ip_client=:#{#logConfig.ipClient}, start_time=:#{#logConfig.startTime}, status=:#{#logConfig.status}, stop_time=:#{#logConfig.stopTime}, target=:#{#logConfig.target}, type=:#{#logConfig.type} " +
            "WHERE id=:#{#logConfig.logId} AND username=:#{#logConfig.username}")
    Mono<Integer> updateCustom(LogConfig logConfig);
}
