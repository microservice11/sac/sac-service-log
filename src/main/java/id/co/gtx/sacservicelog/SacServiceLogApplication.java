package id.co.gtx.sacservicelog;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"id.co.gtx"})
@SpringBootApplication
@OpenAPIDefinition(info =
	@Info(title = "Log Service API",
		version = "${spring.application.version}",
		description = "Documentation Log Service API"))
public class SacServiceLogApplication {

	public static void main(String[] args) {
		SpringApplication.run(SacServiceLogApplication.class, args);
	}

}
