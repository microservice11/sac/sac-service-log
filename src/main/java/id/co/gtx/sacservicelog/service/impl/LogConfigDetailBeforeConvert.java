package id.co.gtx.sacservicelog.service.impl;

import id.co.gtx.sacservicelog.entity.LogConfigDetail;
import org.reactivestreams.Publisher;
import org.springframework.data.r2dbc.mapping.event.BeforeConvertCallback;
import org.springframework.data.relational.core.sql.SqlIdentifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Component
public class LogConfigDetailBeforeConvert implements BeforeConvertCallback<LogConfigDetail> {

    @Override
    public Publisher<LogConfigDetail> onBeforeConvert(LogConfigDetail entity, SqlIdentifier table) {
        entity.setResult(Base64.getEncoder().encodeToString(entity.getResult().getBytes(StandardCharsets.UTF_8)));
        return Mono.just(entity);
    }
}
