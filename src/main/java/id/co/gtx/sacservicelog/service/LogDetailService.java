package id.co.gtx.sacservicelog.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicelog.entity.LogsDetail;
import reactor.core.publisher.Mono;

import java.util.List;

public interface LogDetailService {
    Mono<DtoResponse> findAll();

    Mono<DtoResponse> findById(Long id);

    Mono<DtoResponse<List<LogsDetail>>> findByLogId(Long id);

    Mono<DtoResponse<List<LogsDetail>>> create(List<LogsDetail> logsDetail);

    Mono<DtoResponse> update(Long id, LogsDetail detail);

    Mono<DtoResponse> delete(Long id);
}
