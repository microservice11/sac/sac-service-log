package id.co.gtx.sacservicelog.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicelog.entity.Logs;
import reactor.core.publisher.Mono;

import java.util.List;

public interface LogService {
    Mono<DtoResponse<List<Logs>>> findAll();

    Mono<DtoResponse<Logs>> findById(Long id);

    Mono<DtoResponse<Logs>> create(Logs logs);
}
