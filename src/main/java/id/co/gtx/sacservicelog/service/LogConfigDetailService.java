package id.co.gtx.sacservicelog.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicelog.entity.LogConfigDetail;
import reactor.core.publisher.Mono;

import java.util.List;

public interface LogConfigDetailService {
    Mono<DtoResponse<List<LogConfigDetail>>> findByLogIdAndUsername(Long logId, String username);

    Mono<DtoResponse<LogConfigDetail>> create(LogConfigDetail logConfigDetail);
}
