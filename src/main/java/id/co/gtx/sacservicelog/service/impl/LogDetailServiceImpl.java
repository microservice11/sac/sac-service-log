package id.co.gtx.sacservicelog.service.impl;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacservicelog.entity.Logs;
import id.co.gtx.sacservicelog.entity.LogsDetail;
import id.co.gtx.sacservicelog.repository.LogDetailRepository;
import id.co.gtx.sacservicelog.service.LogDetailService;
import id.co.gtx.sacservicelog.service.LogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

@Service
public class LogDetailServiceImpl implements LogDetailService {

    private final LogDetailRepository logDetailRepository;

    private final LogService logService;

    public LogDetailServiceImpl(LogDetailRepository logDetailRepository,
                                LogService logService) {
        this.logDetailRepository = logDetailRepository;
        this.logService = logService;
    }

    @Override
    public Mono<DtoResponse> findAll() {
        DtoResponse response = new DtoResponse("GET ALL LOGS DETAIL", Status.FAILED, "Data Logs Detail Kosong");

        return logDetailRepository.findAll()
                .collectList()
                .map(logsDetails -> {
                    if (logsDetails.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Logs Detail");
                        response.setData(logsDetails);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse> findById(Long id) {
        DtoResponse response = new DtoResponse("GET LOGS DETAIL", Status.FAILED, "Logs Detail ID " + id + " Tidak Ditemukan");

        return logDetailRepository.findById(id)
                .map(logsDetail -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data Logs Detail By ID " + id);
                    response.setData(logsDetail);
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<LogsDetail>>> findByLogId(Long id) {
        DtoResponse<List<LogsDetail>> response = new DtoResponse<>("GET LOGS DETAIL", Status.FAILED, "Logs ID " + id + " Tidak Ditemukan");

        return logDetailRepository.findByLogId(id)
                .collectList()
                .map(logsDetails -> {
                    if (logsDetails.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Logs Detail By Log ID " + id);
                        response.setData(logsDetails);
                    }
                   return response;
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<List<LogsDetail>>> create(List<LogsDetail> logsDetails) {
        DtoResponse<List<LogsDetail>> response = new DtoResponse<>("SAVE LOGS DETAIL", Status.FAILED, "Gagal Menyimpan Data Logs Detail");
        return logDetailRepository.saveAll(logsDetails)
                .collectList()
                .map(details-> {
                    if (details.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menyimpan Data Logs Detail");
                        response.setData(details);
                    }
                    return response;
                })
                .onErrorResume(e -> {
                    e.printStackTrace();
                    return Mono.just(response);
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse> update(Long id, LogsDetail detail) {
        return findById(id)
                .map(response -> {
                    if (response.getStatus().equals(Status.SUCCESS)) {
                        LogsDetail data = (LogsDetail) response.getData();
                        logService.findById(data.getId()).map(response1 -> {
                            if (response1.getStatus().equals(Status.SUCCESS)) {
                                Logs log = (Logs) response1.getData();
                                data.setColname(detail.getColname());
                                data.setNewValue(detail.getNewValue());
                                data.setOldValue(detail.getOldValue());
                                data.setLog(log);

                                response.setCode("UPDATE LOGS DETAIL");
                                response.setStatus(Status.FAILED);
                                response.setMessage("Gagal Update Data Logs Detail");

                                logDetailRepository.save(data)
                                        .map(logsDetail -> {
                                            if (Optional.of(logsDetail).isPresent()) {
                                                response.setStatus(Status.SUCCESS);
                                                response.setMessage("Berhasil Update Data Logs Detail");
                                                response.setData(logsDetail);
                                            }
                                            return response;
                                        });
                            }
                            return response;
                        });
                    }
                   return response;
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse> delete(Long id) {
        return findById(id)
                .map(response -> {
                   if (response.getStatus().equals(Status.SUCCESS)) {
                       LogsDetail logsDetail = (LogsDetail) response.getData();
                       logDetailRepository.delete(logsDetail);

                       response.setCode("DELETE LOG DETAIL");
                       response.setStatus(Status.SUCCESS);
                       response.setMessage("Berhasil Delete Log Detail");
                       response.setData(logsDetail);
                   }
                   return response;
                });
    }
}
