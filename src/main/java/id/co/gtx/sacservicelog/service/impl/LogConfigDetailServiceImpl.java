package id.co.gtx.sacservicelog.service.impl;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacservicelog.entity.LogConfigDetail;
import id.co.gtx.sacservicelog.repository.LogConfigDetailRepository;
import id.co.gtx.sacservicelog.service.LogConfigDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class LogConfigDetailServiceImpl implements LogConfigDetailService {

    private final LogConfigDetailRepository logConfigDetailRepository;

    public LogConfigDetailServiceImpl(LogConfigDetailRepository logConfigDetailRepository) {
        this.logConfigDetailRepository = logConfigDetailRepository;
    }

    @Override
    public Mono<DtoResponse<List<LogConfigDetail>>> findByLogIdAndUsername(Long logId, String username) {
        DtoResponse<List<LogConfigDetail>> response = new DtoResponse<>("GET LOG CONFIG DETAILS", Status.FAILED, "Gagal Menyimpan Data Log Config Detail");
        return logConfigDetailRepository.findByLogConfigIdAndUsernameOrderByCreateAt(logId, username)
                .collectList()
                .map(logConfigDetails -> {
                    if (logConfigDetails.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Log Config Detail By Log ID: " + logId + " dan Username : " + username);
                        response.setData(logConfigDetails);
                    }
                    return response;
                });
    }

    @Override
    @Transactional
    public Mono<DtoResponse<LogConfigDetail>> create(LogConfigDetail logConfigDetail) {
        DtoResponse<LogConfigDetail> response = new DtoResponse<>("SAVE LOG CONFIG DETAIL", Status.FAILED, "Gagal Menyimpan Data Log Config Detail");
        return logConfigDetailRepository.save(logConfigDetail)
                .flatMap(logDetail -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menyimpan Data Log Config Detail");
                    response.setData(logDetail);
                    return Mono.just(response);
                }).onErrorResume(e -> {
                    e.printStackTrace();
                    return Mono.just(response);
                });
    }
}
