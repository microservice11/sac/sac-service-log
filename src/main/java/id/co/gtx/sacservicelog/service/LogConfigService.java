package id.co.gtx.sacservicelog.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicelog.entity.LogConfig;
import reactor.core.publisher.Mono;

import java.util.List;

public interface LogConfigService {
    Mono<DtoResponse<List<LogConfig>>> findAll();

    Mono<DtoResponse<List<LogConfig>>> findByUsername(String username);

    Mono<DtoResponse<List<LogConfig>>> findLimit(Integer limit);

    Mono<DtoResponse<LogConfig>> findById(Long id, String username);

    Mono<DtoResponse<LogConfig>> create(LogConfig logConfig);

    Mono<DtoResponse<LogConfig>> update(LogConfig logConfig);
}
