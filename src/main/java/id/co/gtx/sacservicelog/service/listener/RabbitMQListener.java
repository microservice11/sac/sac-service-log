package id.co.gtx.sacservicelog.service.listener;

import id.co.gtx.sacmodules.dto.DtoLog;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacservicelog.dto.DtoPayload;
import id.co.gtx.sacservicelog.entity.LogConfig;
import id.co.gtx.sacservicelog.entity.LogConfigDetail;
import id.co.gtx.sacservicelog.entity.Logs;
import id.co.gtx.sacservicelog.repository.LogDetailRepository;
import id.co.gtx.sacservicelog.service.LogConfigDetailService;
import id.co.gtx.sacservicelog.service.LogConfigService;
import id.co.gtx.sacservicelog.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Slf4j
@Component
public class RabbitMQListener {

    private final LogService logService;

    private final LogDetailRepository logDetailRepository;

    private final LogConfigService logConfigService;

    private final LogConfigDetailService logConfigDetailService;

    private final MapperUtil mapperUtil;

    public RabbitMQListener(LogService logService,
                            LogDetailRepository logDetailRepository,
                            LogConfigService logConfigService,
                            LogConfigDetailService logConfigDetailService,
                            MapperUtil mapperUtil) {
        this.logService = logService;
        this.logDetailRepository = logDetailRepository;
        this.logConfigService = logConfigService;
        this.logConfigDetailService = logConfigDetailService;
        this.mapperUtil = mapperUtil;
    }

    @Bean
    public Consumer<DtoPayload<LogConfig>> logConfig() {
        return payload -> {
            LogConfig logConfig = payload.getData();
            log.info(String.valueOf(logConfig));
            if (payload.getOperation().equals("insert")) {
                logConfigService.create(logConfig).block();
            } else if (payload.getOperation().equals("update")) {
                logConfigService.update(logConfig).block();
            }
        };
    }

    @Bean
    public Consumer<LogConfigDetail> logConfigDetail() {
        return logConfigDetail -> {
            log.info(String.valueOf(logConfigDetail));
            logConfigDetailService.create(logConfigDetail).block();
        };
    }

    @Bean
    public Consumer<DtoLog> logData() {
        return dtoLog -> {
            Logs logs = mapperUtil.mappingClass(dtoLog, Logs.class);
            logService.create(logs).block();
        };
    }
}
