package id.co.gtx.sacservicelog.service.impl;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacservicelog.entity.Logs;
import id.co.gtx.sacservicelog.repository.LogRepository;
import id.co.gtx.sacservicelog.repository.query.LogQueryRepository;
import id.co.gtx.sacservicelog.service.LogDetailService;
import id.co.gtx.sacservicelog.service.LogService;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.List;

@Order(1)
@Service
public class LogServiceImpl implements LogService {

    private final LogRepository logRepository;

    private final LogQueryRepository logQueryRepository;

    private final LogDetailService logDetailService;

    public LogServiceImpl(LogRepository logRepository,
                          LogQueryRepository logQueryRepository,
                          @Lazy LogDetailService logDetailService) {
        this.logRepository = logRepository;
        this.logQueryRepository = logQueryRepository;
        this.logDetailService = logDetailService;
    }

    @Override
    public Mono<DtoResponse<List<Logs>>> findAll() {
        DtoResponse<List<Logs>> response = new DtoResponse<>("GET ALL LOGS", Status.FAILED, "Data Logs Kosong");
        return logQueryRepository.findAll()
                .collectList()
                .map(logs -> {
                    if (logs.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Logs");
                        response.setData(logs);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<Logs>> findById(Long id) {
        DtoResponse<Logs> response = new DtoResponse<>("GET LOGS", Status.FAILED, "Log ID " + id + " Tidak Ditemukan");
        return logQueryRepository.findById(id)
                .map(logs -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data Logs By ID");
                    response.setData(logs);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    @Transactional
    public Mono<DtoResponse<Logs>> create(Logs log) {
        DtoResponse<Logs> response = new DtoResponse<>("SAVE LOGS", Status.FAILED, "Gagal Menyimpan Logs");
        if (log.getCreateAt() == null)
            log.setCreateAt(LocalDateTime.now());
        return logRepository.save(log)
                .flatMap(logs -> {
                    logs.getLogsDetails().forEach(logsDetail -> logsDetail.setLogId(logs.getId()));
                    return logDetailService.create(logs.getLogsDetails())
                            .flatMap(resp -> {
                                if (resp.getStatus().equals(Status.SUCCESS)) {
                                    logs.setLogsDetails(resp.getData());
                                    response.setStatus(Status.SUCCESS);
                                    response.setMessage("Berhasil Menyimpan Logs");
                                    response.setData(logs);
                                }
                                return Mono.just(response);
                            });
                })
                .onErrorResume(e -> {
                    e.printStackTrace();
                    return Mono.just(response);
                });
    }
}
