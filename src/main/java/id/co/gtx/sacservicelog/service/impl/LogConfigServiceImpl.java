package id.co.gtx.sacservicelog.service.impl;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacservicelog.entity.LogConfig;
import id.co.gtx.sacservicelog.repository.LogConfigRepository;
import id.co.gtx.sacservicelog.repository.query.LogConfigQueryRepoitory;
import id.co.gtx.sacservicelog.service.LogConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Service
public class LogConfigServiceImpl implements LogConfigService {

    private final LogConfigRepository logConfigRepository;

    private final LogConfigQueryRepoitory logConfigQueryRepoitory;

    public LogConfigServiceImpl(LogConfigRepository logConfigRepository,
                                LogConfigQueryRepoitory logConfigQueryRepoitory) {
        this.logConfigRepository = logConfigRepository;
        this.logConfigQueryRepoitory = logConfigQueryRepoitory;
    }

    @Override
    public Mono<DtoResponse<List<LogConfig>>> findAll() {
        DtoResponse<List<LogConfig>> response = new DtoResponse<>("GET ALL LOG CONFIG", Status.FAILED, "Data Log Config Kosong");
        return logConfigQueryRepoitory.findAll()
                .collectList()
                .map(logConfigs -> {
                    if (logConfigs.size() > 0) {
                        response.setStatus(Status.FAILED);
                        response.setMessage("Berhasil Menampilkan Data Log Config");
                        response.setData(logConfigs);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<LogConfig>>> findByUsername(String username) {
        DtoResponse<List<LogConfig>> response = new DtoResponse<>("GET LOG CONFIG", Status.FAILED, "Data Log Config Tidak Ditemukan By Username: " + username);
        return logConfigQueryRepoitory.findByUsernameAndType(username, 1)
                .collectList()
                .map(logConfigs -> {
                    if (logConfigs.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan Data Log Config By Username: " + username);
                        response.setData(logConfigs);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<List<LogConfig>>> findLimit(Integer limit) {
        DtoResponse<List<LogConfig>> response = new DtoResponse<>("GET LIMIT LOG CONFIG", Status.FAILED, "Data Log Config Kosong");
        return logConfigQueryRepoitory.findAll(limit)
                .collectList()
                .map(logConfigs -> {
                    if (logConfigs.size() > 0) {
                        response.setStatus(Status.SUCCESS);
                        response.setMessage("Berhasil Menampilkan " + limit + " Data Log Config");
                        response.setData(logConfigs);
                    }
                    return response;
                });
    }

    @Override
    public Mono<DtoResponse<LogConfig>> findById(Long id, String username) {
        DtoResponse<LogConfig> response = new DtoResponse<>("GET LOG CONFIG", Status.FAILED, "Log Config ID " + id + " dan Username " + username + " Tidak Ditemukan");
        return logConfigQueryRepoitory.findByLogIdAndUsername(id, username)
                .map(logConfig -> {
                    response.setStatus(Status.SUCCESS);
                    response.setMessage("Berhasil Menampilkan Data Log Config By ID");
                    response.setData(logConfig);
                    return response;
                })
                .switchIfEmpty(Mono.just(response));
    }

    @Override
    @Transactional
    public Mono<DtoResponse<LogConfig>> create(LogConfig logConfig) {
        DtoResponse<LogConfig> response = new DtoResponse<>("SAVE LOG CONFIG", Status.FAILED, "Gagal Menyimpan Log Config");
        return Mono.just(logConfig)
                .flatMap(logs -> logConfigRepository.saveCustom(logs)
                        .flatMap(count -> {
                            if (count > 0) {
                                response.setStatus(Status.SUCCESS);
                                response.setMessage("Berhasil Menyimpan Log Config");
                                response.setData(logs);
                            }
                            return Mono.just(response);
                        })
                        .onErrorResume(e -> {
                            e.printStackTrace();
                            return Mono.just(response);
                        })
                );
    }

    @Override
    @Transactional
    public Mono<DtoResponse<LogConfig>> update(LogConfig logConfig) {
        DtoResponse<LogConfig> response = new DtoResponse<>("UPDATE LOG CONFIG", Status.FAILED, "Gagal Update Log Config");
        return Mono.just(logConfig)
                .flatMap(logs -> logConfigRepository.updateCustom(logs)
                        .flatMap(count -> {
                            if (count > 0) {
                                response.setStatus(Status.SUCCESS);
                                response.setMessage("Berhasil Update Log Config");
                                response.setData(logs);
                            }
                            return Mono.just(response);
                        })
                        .onErrorResume(e -> {
                            e.printStackTrace();
                            return Mono.just(response);
                        })
                );
    }
}
