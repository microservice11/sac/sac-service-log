package id.co.gtx.sacservicelog.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DtoPayload<T> implements Serializable {
    private static final long serialVersionUID = 2691885114312238922L;

    private String operation;

    private T data;
}
