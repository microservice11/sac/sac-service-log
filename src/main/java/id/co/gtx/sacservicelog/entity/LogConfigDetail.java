package id.co.gtx.sacservicelog.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacmodules.enumz.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("log_config_detail")
public class LogConfigDetail implements Serializable {
    private static final long serialVersionUID = -7622585564754087397L;

    @Id
    private Long id;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long[] ids;

    private String target;

    private String action;

    private Status status;

    private String cmd;

    private String result;

    @Column("log_config_id")
    @JsonProperty("log_config_id")
    private Long logConfigId;

    private String username;

    @Transient
    @JsonBackReference
    @JsonProperty("log_config")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LogConfig logConfig;

    @Column("create_at")
    @JsonProperty("create_at")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createAt;
}
