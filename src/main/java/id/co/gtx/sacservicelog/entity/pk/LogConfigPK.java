package id.co.gtx.sacservicelog.entity.pk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LogConfigPK implements Serializable {
    private static final long serialVersionUID = 6315280467335600636L;

    @JsonProperty("log_id")
    private Long logId;
    private String username;
}
