package id.co.gtx.sacservicelog.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacmodules.enumz.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("log_config")
public class LogConfig implements Serializable {
    private static final long serialVersionUID = 6879021854391552057L;

    @Column("id")
    @JsonProperty("id")
    private long logId;

    private String username;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long[] ids;

    private String target;

    private String action;

    private Status status;

    private Integer type;

    @JsonProperty("ip_client")
    @Column("ip_client")
    private String ipClient;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<LogConfigDetail> logConfigDetails;

    @Column("start_time")
    @JsonProperty("start_time")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime startTime;

    @Column("stop_time")
    @JsonProperty("stop_time")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime stopTime;
}
