package id.co.gtx.sacservicelog.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("logs_detail")
public class LogsDetail implements Serializable {
    private static final long serialVersionUID = 6053233402920403900L;

    @Id
    private Long id;

    private String colname;

    @Column("new_value")
    @JsonProperty("new_value")
    private String newValue;

    @Column("old_value")
    @JsonProperty("old_value")
    private String oldValue;

    @Column("log_id")
    @JsonProperty("log_id")
    private Long logId;

    @Transient
    @JsonBackReference
    @JsonProperty("log")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Logs log;
}
