package id.co.gtx.sacservicelog.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("logs")
public class Logs implements Serializable {
    private static final long serialVersionUID = -2938202692930381989L;

    @Id
    private Long id;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long[] ids;

    private String message;

    private String operation;

    private String tbname;

    private String tbkey;

    @JsonProperty("ip_client")
    @Column("ip_client")
    private String ipClient;

    private String username;

    @Transient
    @JsonProperty("log_details")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<LogsDetail> logsDetails;

    @Column("create_at")
    @JsonProperty("create_at")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss[.SSS]")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createAt;
}
